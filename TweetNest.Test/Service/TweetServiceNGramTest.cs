﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TweetNest;
using System.Collections.Generic;
using System.Linq;
using TweetNest.Model;
using TweetNest.Service;
using TweetNest.Util;
using Nest;

namespace SearchService.Tests
{
    [TestClass]
    public class TweetServiceNGramTest
    {
        public ElasticClient client { get; set; }

        [TestInitialize]
        public void Setup()
        {
            client = Client.ForIndex(TwitterService.ElasticSearchIndex);
            var twitterService = new TwitterService();
            twitterService.CreateIndexWithNgrams();
        }

        [TestCleanup]
        public void TearDown() {
            var twitterService = new TwitterService();
            twitterService.DeleteIndex();
        }


        [TestMethod]
        public void TestNGramSearchTweets()
        {
            var tweetService = new TweetService();
            tweetService.PostTweet(2, "IADNUG", "1st tweet from Unit Test " + DateTime.Now);
            tweetService.PostTweet(3, "IADNUG", "2nd tweet from DNUG Unit Test " + DateTime.Now);

            // elasticsearch is near real-time, not real-time
            System.Threading.Thread.Sleep(500);
            client.Refresh(TwitterService.ElasticSearchIndex);

            IEnumerable<Tweet> tweets = tweetService.SearchTweetsByTextQuery("DN");

            Assert.IsTrue(tweets.Any(), "Expected to find tweet(s) with the partial word DN in the tweet!");
            Assert.IsTrue(tweets.Count() == 1, "Expected one tweet with the DN ngram");

            int counter = 0;
            foreach (var t in tweets)
            {
                if (t.Message.Contains("DNUG"))
                    counter++;
            }
            Assert.IsTrue(counter == 1, "Expected one tweet with DNUG in the message");
        }


    }
}
