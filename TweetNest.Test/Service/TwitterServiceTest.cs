﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nest;
using TweetNest.Service;
using TweetNest.Util;

namespace TweetNest.Test
{
    [TestClass]
    public class TwitterServiceTest
    {
        public ElasticClient client { get; set; }

        [TestInitialize]
        public void Setup() {
            client = Client.ForIndex(TwitterService.ElasticSearchIndex);
        }

        [TestCleanup]
        public void TearDown() {
            client.DeleteIndex(TwitterService.ElasticSearchIndex);
        }

        [TestMethod]
        public void TestCreateIndex()
        {
            var twitterService = new TwitterService();

            bool deleteOk = twitterService.DeleteIndex();
            var deleteResponse = client.DeleteIndex(TwitterService.ElasticSearchIndex);
            var indexResponse = client.GetIndexSettings(TwitterService.ElasticSearchIndex);
            Assert.IsFalse(indexResponse.IsValid);

            bool createOk = twitterService.CreateIndex();

            Assert.IsTrue(createOk, "Expected Index to be created");

            var r = client.GetIndexSettings(TwitterService.ElasticSearchIndex);
            Assert.IsTrue(r.IsValid, "Expected the verification of the index creation to match OK");
        }


        [TestMethod]
        public void TestDeleteIndex()
        {

            var indexResponse = client.GetIndexSettings(TwitterService.ElasticSearchIndex);
            if (!indexResponse.IsValid)
            {
                var settings = new IndexSettings();
                var createResponse = client.CreateIndex(TwitterService.ElasticSearchIndex, settings);
                Assert.IsTrue(createResponse.OK, "Unable to setup the test properly");
            }

            var twitterService = new TwitterService();
            bool ok = twitterService.DeleteIndex();

            Assert.IsTrue(ok, "Expected Index to be deleted");

            var r = client.GetIndexSettings(TwitterService.ElasticSearchIndex);
            Assert.IsFalse(r.IsValid, "Expected the index to not exist");
        }
    }
}
