﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TweetNest;
using System.Collections.Generic;
using System.Linq;
using TweetNest.Model;
using TweetNest.Service;
using TweetNest.Util;
using Nest;

namespace SearchService.Tests
{
    [TestClass]
    public class TweetServiceTest
    {

        public ElasticClient client { get; set; }

        [TestInitialize]
        public void Setup() {
            client = Client.ForIndex(TwitterService.ElasticSearchIndex);
            var twitterService = new TwitterService();
            twitterService.CreateIndex();
        }

        [TestCleanup]
        public void TearDown() {
            var twitterService = new TwitterService();
            twitterService.DeleteIndex();
        }

        [TestMethod]
        public void TestIndexTweet()
        {
            var tweetService = new TweetService();
            tweetService.PostTweet(1, "IADNUG", "From Unit Test " + DateTime.Now);
            System.Threading.Thread.Sleep(500);
            client.Refresh(TwitterService.ElasticSearchIndex);

            Tweet t = tweetService.GetTweetById(1);
            Assert.IsNotNull(t, "Expected to find the tweet in the index");
            Assert.IsTrue(t.User.Equals("IADNUG"), "Expected it to be tweeted by IADNUG");
        }

        [TestMethod]
        public void TestGetAllTweets()
        {
            var tweetService = new TweetService();
            tweetService.PostTweet(2, "IADNUG", "1st tweet from Unit Test " + DateTime.Now);
            tweetService.PostTweet(3, "IADNUG", "2nd tweet from Unit Test " + DateTime.Now);

            // elasticsearch is near real-time, not real-time
            System.Threading.Thread.Sleep(500);
            client.Refresh(TwitterService.ElasticSearchIndex);

            IEnumerable<Tweet> tweets = tweetService.GetAllTweets();

            Assert.IsTrue(tweets.Any(), "Expected at least one tweet!");
            Assert.IsTrue(tweets.Count() >= 2, "Expected at least two tweets");

            int counter = 0;
            foreach (var t in tweets)
            {
                if (t.User.Equals("IADNUG"))
                    counter++;
            }
            Assert.IsTrue(counter >= 2, "Expected two tweets by the IADNUG");
        }


        [TestMethod]
        public void TestSearchTweets()
        {
            var tweetService = new TweetService();
            tweetService.PostTweet(2, "IADNUG", "1st tweet from Unit Test " + DateTime.Now);
            tweetService.PostTweet(3, "IADNUG", "2nd tweet from Unit Test " + DateTime.Now);

            // elasticsearch is near real-time, not real-time
            System.Threading.Thread.Sleep(500);
            client.Refresh(TwitterService.ElasticSearchIndex);

            IEnumerable<Tweet> tweets = tweetService.SearchTweetsByMessage("Unit");

            Assert.IsTrue(tweets.Any(), "Expected to find tweet(s) with the word Unit in the tweet!");
            Assert.IsTrue(tweets.Count() >= 2, "Expected at least two tweets with the word Unit");

            int counter = 0;
            foreach (var t in tweets)
            {
                if (t.Message.Contains("Unit"))
                    counter++;
            }
            Assert.IsTrue(counter >= 2, "Expected two tweets with Unit in the message");
        }
    }
}
