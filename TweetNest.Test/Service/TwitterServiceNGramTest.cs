﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nest;
using TweetNest.Service;
using TweetNest.Util;

namespace TweetNest.Test
{
    [TestClass]
    public class TwitterServiceNGramTest
    {
        public ElasticClient client { get; set; }

        [TestInitialize]
        public void Setup() {
            client = Client.ForIndex(TwitterService.ElasticSearchIndex);
        }

        [TestCleanup]
        public void TearDown() {
            client.DeleteIndex(TwitterService.ElasticSearchIndex);
        }

        [TestMethod]
        public void TestCreateIndexWithNgrams()
        {
            var twitterService = new TwitterService();

            bool deleteOk = twitterService.DeleteIndex();
            var deleteResponse = client.DeleteIndex(TwitterService.ElasticSearchIndex);
            var indexResponse = client.GetIndexSettings(TwitterService.ElasticSearchIndex);
            Assert.IsFalse(indexResponse.IsValid);

            bool createOk = twitterService.CreateIndexWithNgrams();

            Assert.IsTrue(createOk, "Expected Index to be created");

            var r = client.GetIndexSettings(TwitterService.ElasticSearchIndex);
            Assert.IsTrue(r.IsValid, "Expected the verification of the index creation to match OK");
        }

    }
}
