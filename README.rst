================================
TweetNest
================================

The repository contains examples of using the NEST client to elasticsearch.

* Create/delete an index (twitter)
* Index a type (tweet)
* Search for tweets by message content
* Search for tweets via NGrams