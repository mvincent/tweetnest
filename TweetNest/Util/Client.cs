﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace TweetNest.Util
{
    public class Client
    {
        /// <summary>
        /// Return a new client
        /// </summary>
        /// <param name="indexName">name of the index</param>
        /// <returns>a client pointing to http://localhost:9200/index </returns>
        public static ElasticClient ForIndex(String indexName)
        {
            ElasticClient client;

            String hostname = "localhost";
            int port = 9200;

            var elasticSettings = new ConnectionSettings(hostname, port)
                          .SetDefaultIndex(indexName);
            client = new ElasticClient(elasticSettings);

            ConnectionStatus connectionStatus;
            if (!client.TryConnect(out connectionStatus))
            {
                throw new Exception("client not ready");
            }

            return client;
        }
    }
}
