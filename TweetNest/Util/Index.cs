﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using TweetNest.Model;

namespace TweetNest.Util
{
    public class Index
    {
        public static IIndicesOperationResponse MapAndAnalyze(
            ElasticClient client,
            String indexName,
            Func<AnalysisDescriptor, AnalysisDescriptor> analysisSelector,
            Func<RootObjectMappingDescriptor<Tweet>, RootObjectMappingDescriptor<Tweet>> typeMappingDescriptor
            )
        {

            var typeMapping = new RootObjectMapping();

            var result = client.CreateIndex(indexName, c => c
                .NumberOfReplicas(1)
                .NumberOfShards(1)
                .Analysis(analysisSelector)
                .AddMapping<Tweet>(typeMappingDescriptor)
            );

            return result;
        }
    }
}
