﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TweetNest.Model
{
    /// <summary>
    /// Represents a user account within the twitter system
    /// </summary>
    public class User
    {
        /// <summary>
        /// A unqiue username
        /// </summary>
        public String Name { get; set; }
    }
}
