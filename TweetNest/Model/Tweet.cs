﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TweetNest.Model
{
    /// <summary>
    /// Tweet POCO
    /// </summary>
    public class Tweet
    {
        public long Id { get; set; }
        public String User { get; set; }
        public DateTime PostDate { get; set; }
        public String Message { get; set; }
    }
}
