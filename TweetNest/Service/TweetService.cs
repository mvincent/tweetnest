﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using TweetNest.Model;
using TweetNest.Util;

namespace TweetNest.Service
{
    /// <summary>
    /// Tweets and searches for tweets
    /// </summary>
    public class TweetService
    {
        public ElasticClient client { get; set; }

        public TweetService()
        {
            client = Client.ForIndex(TwitterService.ElasticSearchIndex);
        }

        /// <summary>
        /// Retrieve all tweet documents from the twitter index
        /// </summary>
        /// <remarks>
        /// curl -XGET localhost:9200/twitter/tweets/_search
        /// </remarks>
        public IEnumerable<Tweet> GetAllTweets() {
            if (client.IsValid)
            {
                var result = client.Search<Tweet>(s => s
                    .Index(TwitterService.ElasticSearchIndex)
                    .Type("tweets") // turns into "tweets"
                    .MatchAll()
                );

                return result.Documents;

            }
            return new List<Tweet>();
        }

        /// <summary>
        /// Search for all tweets with <code>queryString</code> in their message.
        /// </summary>
        /// <remarks>
        /// curl -XGET 'localhost:9200/twitter/tweets/_search?q=message:queryString'
        /// 
        /// 
        /// {StatusCode: OK, 
        ///     Method: POST, 
        ///     Url: http://localhost:9200/twitter/tweets/_search, 
        ///     Request: {
        ///   "query": {
        ///     "text": {
        ///       "message": {
        ///         "type": "phrase",
        ///         "query": "queryString"
        ///       }
        ///     }
        ///   }
        //}, 
        /// </remarks>
        public IEnumerable<Tweet> SearchTweetsByMessage(String queryString)
        {
            if (client.IsValid)
            {
                var result = client.
                    Search<Tweet>(s => s
                        .Index(TwitterService.ElasticSearchIndex)
                        .Type("tweets")
                        .Query(q=>q
                            .TextPhrase(t=>t
                                .OnField(f=>f.Message)
                                .QueryString(queryString)
                            )
                        )
                    );

                return result.Documents;

            }
            return new List<Tweet>();
        }



        /// <summary>
        /// Search for all tweets with <code>queryString</code> in their message.
        /// </summary>
        /// <remarks>
        /// curl -XGET http://localhost:9200/twitter/_search?pretty=1 -d '{"query":{"text":{"message.ngram":"DN"}}}'
        /// </remarks>
        public IEnumerable<Tweet> SearchTweetsByTextQuery(String queryString)
        {
            if (client.IsValid)
            {
                var result = client.
                    Search<Tweet>(s => s
                        .Index(TwitterService.ElasticSearchIndex)
                        .Type("tweets")
                        .Query(q => q
                            .Text(t=>t.OnField("message.ngram").QueryString(queryString))
                        )
                    );

                return result.Documents;

            }
            return new List<Tweet>();
        }


        /// <summary>
        /// Get a tweet by it's unique identifier
        /// </summary>
        /// <remarks>
        /// curl -XGET localhost:9200/twitter/tweets/2
        /// </remarks>
        public Tweet GetTweetById(int documentId)
        {
            if (!client.IsValid)
                throw new Exception("Unable to connect");

            var queryResult = client.GetFull<Tweet>(s => s
                .Index(TwitterService.ElasticSearchIndex) 
                .Type("tweets") 
                .Id(documentId)
            );

            return queryResult.Source;
        }



        /// <summary>
        /// Post a new tweet to the "twitter" index.
        /// </summary>
        /// <remarks>
        /// curl -XPUT localhost:9200/twitter/tweets/2 -d '
        /// {
        ///     user : "iadnug",
        ///     message: "Tweet two: elasticsearch may solve a few problems for me!",
        ///     postDate: "20130502T02:00:00"
        /// }'
        /// </remarks>
        public void PostTweet(long id, String userName, String message)
        {
            var tweet = new Tweet { Id = id, User = userName, Message = message, PostDate = DateTime.Now };
            if(client.IsValid)
                client.IndexAsync(tweet);
           
        }
    }
}
