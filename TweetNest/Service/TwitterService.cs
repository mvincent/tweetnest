﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using TweetNest.Model;
using TweetNest.Util;

namespace TweetNest.Service
{
    /// <summary>
    /// Service to manage interactions with the "twitter" elasticsearch index
    /// </summary>
    public class TwitterService
    {
        public static String ElasticSearchIndex = "twitter";

        public ElasticClient client { get; set; }

        public TwitterService()
        {
            client = Client.ForIndex(ElasticSearchIndex);
        }

        /// <summary>
        /// Create the "twitter" index
        /// </summary>
        /// <returns>true if created</returns>
        public bool CreateIndex() 
        {
            if (!client.IsValid)
                throw new Exception("Unable to connect");

            var settings = new IndexSettings();

            var createResponse = this.client.CreateIndex(ElasticSearchIndex, settings);
            return createResponse.OK;
        }

        /// <summary>
        /// Create the "twitter" index
        /// </summary>
        /// <returns>true if created</returns>
        /// <remarks>
        /// curl -XPUT 'http://127.0.0.1:9200/twitter/?pretty=1'  -d '
        /// {
        ///    "mappings" : {
        ///       "tweet" : {
        ///          "properties" : {
        ///             "message" : {
        ///                "type": "multi_field",
        ///                "fields" : {
        ///                   "untouched": {
        ///                      "type": "string",
        ///                      "index": "not_analyzed"
        ///                   },
        ///                   "message": {
        ///                      "type": "string",
        ///                      "analyzer": "standard_message"
        ///                   },
        ///                   "ngram" : {
        ///                      "search_analyzer": "standard_message",
        ///                      "index_analyzer": "partial_message",
        ///                      "type": "string"
        ///                   }
        ///                }
        ///             }
        ///          }
        ///       }
        ///    },
        ///    "settings" : {
        ///       "analysis" : {
        ///          "filter" : {
        ///             "message_ngrams" : {
        ///                "side" : "front",
        ///                "max_gram" : 10,
        ///                "min_gram" : 1,
        ///                "type" : "edgeNGram"
        ///             }
        ///          },
        ///          "analyzer" : {
        ///             "standard_message" : {
        ///                "filter" : [
        ///                   "standard",
        ///                   "lowercase",
        ///                   "asciifolding"
        ///                ],
        ///                "type" : "custom",
        ///                "tokenizer" : "standard"
        ///             },
        ///             "partial_message" : {
        ///                "filter" : [
        ///                   "standard",
        ///                   "lowercase",
        ///                   "asciifolding",
        ///                   "message_ngrams"
        ///                ],
        ///                "type" : "custom",
        ///                "tokenizer" : "standard"
        ///             }
        ///          }
        ///       }
        ///    }
        /// }'
        /// </remarks>
        public bool CreateIndexWithNgrams()
        {
            if (!client.IsValid)
                throw new Exception("Unable to connect");

            var result = Index.MapAndAnalyze(
                client,
                TwitterService.ElasticSearchIndex,
                a => a
                    .TokenFilters(tf => tf
                        .Add(
                            "message_ngrams",
                            new EdgeNGramTokenFilter
                            {
                                MaxGram = 10,
                                MinGram = 1,
                                Side = "front"
                            }

                        )
                    )
                    .Analyzers(an => an
                        .Add(
                            "standard_message",
                            new CustomAnalyzer
                            {
                                Tokenizer = "standard",
                                Filter = new List<String>
                                {
                                    "standard",
                                    "lowercase",
                                    "asciifolding"
                                }
                            }
                        )
                        .Add("partial_message",
                            new CustomAnalyzer
                            {
                                Tokenizer = "standard",
                                Filter = new List<String>
                                {
                                    "standard",
                                    "lowercase",
                                    "asciifolding",
                                    "message_ngrams"
                                }
                            }
                        )
                    ),
                m => m
                    .Properties(
                        p => p
                            .MultiField(mf => mf
                                .Name(tweet => tweet.Message)
                                .Fields(mff => mff
                                    .Generic(g => g
                                        .Name("untouched")
                                        .Type("string")
                                        .Index("not_analyzed")
                                    )
                                    .Generic(g => g
                                        .Name("message")
                                        .Type("string")
                                        .IndexAnalyzer("standard_message")
                                    )
                                    .Generic(g => g
                                        .Name("ngram")
                                        .Type("string")
                                        .IndexAnalyzer("partial_message")
                                        .SearchAnalyzer("standard_message")
                                    )
                            )
                        )
                    )
            );

            return result.OK;
        }

        /// <summary>
        /// Delete the "twitter" index
        /// </summary>
        /// <returns>true if deleted</returns>
        public bool DeleteIndex()
        {
            if (!client.IsValid)
                throw new Exception("Unable to connect");

            var createResponse = this.client.DeleteIndex(ElasticSearchIndex);

            return createResponse.OK;
        }
    }
}
